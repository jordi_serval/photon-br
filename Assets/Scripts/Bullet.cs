﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private int damage;
    private int attacketId;
    private bool isMine;

    public Rigidbody rig;

    public void Initialize (int damage, int attacketId, bool isMine)
    {
        this.damage = damage;
        this.attacketId = attacketId;
        this.isMine = isMine;

        Destroy(gameObject, 5.0f);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player") && isMine)
        {
            PlayerController player = GameManager.instance.GetPlayer(other.gameObject);

            if (player.id != attacketId)
                player.photonView.RPC("TakeDamage", player.photonPlayer, attacketId, damage);
        }

        Destroy(gameObject);
    }
}
