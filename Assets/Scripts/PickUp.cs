﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public enum PickUpType
{
    Health,
    Ammo
}

public class PickUp : MonoBehaviour
{
    public PickUpType type;
    public int value;

    private void OnTriggerEnter(Collider other)
    {
        if (!PhotonNetwork.IsMasterClient)
            return;

        if (other.CompareTag("Player"))
        {
            PlayerController player = GameManager.instance.GetPlayer(other.gameObject);

            if (type == PickUpType.Health)
                player.photonView.RPC("Heal", player.photonPlayer, value);
            else if (type == PickUpType.Ammo)
                player.photonView.RPC("GiveAmmo", player.photonPlayer, value);

            PhotonNetwork.Destroy(gameObject);
        }
    }
}
